import React from "react";
import {
  BrowserRouter as Router,
  Routes as Switch,
  Route,
} from "react-router-dom";
import styled from "styled-components";
import GlobalPage from "./Pages/GlobalPage";

type Props = {};

const routes = (
  <>
    <Route index element={<GlobalPage />} />;
  </>
);

const App = (props: Props) => {
  return (
    <GlobalAppContainer>
      <Router>
        <Switch>{routes}</Switch>
      </Router>
    </GlobalAppContainer>
  );
};

const GlobalAppContainer = styled.div`
  height: 100vh;
  position: relative;
`;

export default App;
