import React from "react";
import FooterContent from "../components/Container/Footer/FooterContent";

type Props = {};

const FooterPage = (props: Props) => {
  return (
    <div>
      <FooterContent />
    </div>
  );
};

export default FooterPage;
