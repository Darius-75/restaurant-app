import React from "react";
import OrderContent from "../components/Container/Order/OrderContent";

type Props = {};

const OrderPage = (props: Props) => {
  return (
    <div>
      <OrderContent />
    </div>
  );
};

export default OrderPage;
