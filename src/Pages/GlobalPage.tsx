import React from "react";
import FooterContent from "../components/Container/Footer/FooterContent";
import styled from "styled-components";
import Navbar from "../components/Container/Navbar/NavbarContent";
import HomeContent from "../components/Container/Home/HomeContent";
import MenuContent from "../components/atoms/wrapper/Menu/MenuContent";
import OrderPage from "./OrderPage";

type Props = {};

const FooterPage = (props: Props) => {
  return (
    <div>
      <DisplayPages>
        <Navbar />
        <HomeContent />
        <MenuContent />
        <OrderPage />
        <FooterContent />
      </DisplayPages>
    </div>
  );
};

const DisplayPages = styled.div`
  display: flex;
  flex-direction: column;
`;

export default FooterPage;
