import React from "react";
import MenuContent from "../components/atoms/wrapper/Menu/MenuContent";

type Props = {};

const MenuPage = (props: Props) => {
  return (
    <div>
      <MenuContent />
    </div>
  );
};

export default MenuPage;
