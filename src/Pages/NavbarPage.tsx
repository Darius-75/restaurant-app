import React from "react";
import NavbarContent from "../components/Container/Navbar/NavbarContent";

type Props = {};

const NavbarPage = (props: Props) => {
  return (
    <div>
      <NavbarContent />
    </div>
  );
};

export default NavbarPage;
