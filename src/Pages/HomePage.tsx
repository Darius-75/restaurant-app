import React from "react";
import HomeContent from "../components/Container/Home/HomeContent";

type Props = {};

const HomePage = (props: Props) => {
  return (
    <div>
      <HomeContent />
    </div>
  );
};

export default HomePage;
