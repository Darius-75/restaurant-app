import React from "react";
import styled from "styled-components";
import { colors, size } from "../../../Variables/globalStyles";
import Text from "../../atoms/Text/Text";
import Icon from "../../atoms/Icon/Icon";
import menu1 from "../../../img/menu1.jpg";
import menu2 from "../../../img/menu1.jpg";
import menu3 from "../../../img/menu1.jpg";
import { GiScooter } from "react-icons/gi";
const { Primary, Secondary } = colors;
const { XLMax, Max, Min, Normal } = size;
type Props = {
  image: any;
  title: string;
  price: string;
};

const MenuSection = ({ image, title, price }: Props) => {
  return (
    <div>
      <GlobalMenuSectionDisplay>
        <MenuTopSection>
          <Icon src={image} width="100%" height="100%" />
        </MenuTopSection>
        <MenuBotSection>
          <TitleBotSextionDisplay>
            <Text value={title} fontWeight="bold" />
            <Text value={price} color={"#E15151"} fontWeight="bold" />
          </TitleBotSextionDisplay>
          <MiddleTextDisplay>
            <Text
              value={
                "lorem ipsum  is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummymen book. "
              }
            />
          </MiddleTextDisplay>
          <DeliveryDisplayTextAndIcon>
            <Text value={"Order a delivery"} fontWeight="bold" />
            <GiScooter fontWeight="bold" />
          </DeliveryDisplayTextAndIcon>
        </MenuBotSection>
      </GlobalMenuSectionDisplay>
    </div>
  );
};

const GlobalMenuSectionDisplay = styled.div`
  width: 250px;
  height: 100%;
  display: flex;
  flex-direction: column;
`;
const MenuTopSection = styled.div`
  width: 100%;
  height: 40%;
  object-fit: contain;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  overflow: hidden;
`;
const MenuBotSection = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: #d9d9d9;
  padding: 10px;
  box-sizing: border-box;
`;
const TitleBotSextionDisplay = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const MiddleTextDisplay = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  text-align: left;
`;
const DeliveryDisplayTextAndIcon = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;
export default MenuSection;
