import React from "react";

type Props = {};

const BookingFormLast = (props: Props) => {
  return <div>BookingFormLast</div>;
};

export default BookingFormLast;

// import React from "react";
// import styled from "styled-components";
// import { colors, size } from "../../../Variables/globalStyles";
// import tee3 from "../../../img/tee3.jpg";
// import Spacer from "../../atoms/Spacer/Spacer";
// import Text from "../../atoms/Text/Text";
// import Input from "../../atoms/Input/Input";
// import Button from "../../atoms/Button/Button";

// const { Primary, Secondary } = colors;
// const { XLMax, Max, Min, Normal } = size;
// type Props = {};

// const BookingContent = (props: Props) => {
//   return (
//     <GlobalBookingDisplay>
//       <OrderContentDisplay>
//         <Text value={"Order A Table "} size={XLMax} color={Secondary} />
//         <OrderGridDisplay>
//           <form>
//             <Input
//               type="Ocassion"
//               placeholder=" Ocassion"
//               width="max-content"
//               height="max-content"
//               backgroundColor="#fff"
//             />

//             <Input
//               type="Temps"
//               placeholder="  Temps"
//               width="max-content"
//               height="max-content"
//               backgroundColor="#fff"
//             />
//             <Input
//               type="text"
//               placeholder="Nombre d'invités"
//               width="max-content"
//               height="max-content"
//               backgroundColor="#fff"
//             />
//             <Input
//               type="Date"
//               placeholder=" Date"
//               width="max-content"
//               height="max-content"
//               backgroundColor="#fff"
//               // onChange={(e: any) => {
//               //   const selectedDate = new Date(e.target.value);
//               //   fetchData(selectedDate);
//               // }}
//             />
//             <Input
//               type="Hour"
//               placeholder="Select Time"
//               width="max-content"
//               height="max-content"
//               backgroundColor="#fff"
//               // options={availableTimes}
//             />
//           </form>
//         </OrderGridDisplay>
//         <Button
//           color={Secondary}
//           padding="5px 10px"
//           borderRadius="5px"
//           cursor="pointer"
//           // handleClick={() => {
//           //   handleSubmit(formData);
//           // }}
//         >
//           <Text value={" Faite votre réservation"} size={Max} color={"#000"} />
//         </Button>
//       </OrderContentDisplay>
//     </GlobalBookingDisplay>
//   );
// };

// const GlobalBookingDisplay = styled.div`
//   height: 100vh;
//   width: 100%;
//   display: flex;
//   justify-content: center;
//   align-items: center;
// `;

// const OrderContentDisplay = styled.div`
//   height: 70%;
//   width: 50%;
//   background-color: ${Primary};
//   display: flex;
//   flex-direction: column;
//   border-radius: 10px;
//   justify-content: space-around;
//   align-items: center;
//   text-decoration: none;
// `;

// const OrderGridDisplay = styled.div`
//   display: grid;
//   height: 50%;
//   width: 50%;
//   grid-template-columns: repeat(2, 1fr); /* 2 colonnes de largeur égale */
//   grid-template-rows: repeat(3, 1fr); /* 3 lignes de hauteur égale */
//   align-items: center;
//   justify-items: center;
// `;

// export default BookingContent;
