import React, { useState } from "react";
import styled from "styled-components";
import { colors, size } from "../../../Variables/globalStyles";
import tee3 from "../../../img/tee3.jpg";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import Input from "../../atoms/Input/Input";
import Button from "../../atoms/Button/Button";

const { Primary, Secondary } = colors;

const BookingForm = ({ availableTimes, handleDateChange }) => {
  return (
    <form>
      <label htmlFor="res-date">Choose date</label>
      <input type="date" id="res-date" onChange={handleDateChange} />
      <label htmlFor="res-time">Choose time</label>
      <select id="res-time">
        {availableTimes.map((timeSlot) => (
          <option key={timeSlot.time} value={timeSlot.time}>
            {timeSlot.time}
          </option>
        ))}
      </select>
      <label htmlFor="guests">Number of guests</label>
      <input type="number" placeholder="1" min="1" max="10" id="guests" />
      <label htmlFor="occasion">Occasion</label>
      <select id="occasion">
        <option>Birthday</option>
        <option>Anniversary</option>
      </select>
      <input type="submit" value="Make Your reservation" />
    </form>
  );
};

const GlobalBookingDisplay = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const OrderContentDisplay = styled.div`
  height: 70%;
  width: 50%;
  background-color: ${Primary};
  display: flex;
  flex-direction: column;
  border-radius: 10px;
  justify-content: space-around;
  align-items: center;
  text-decoration: none;
`;

const OrderGridDisplay = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  justify-items: center;
`;
export default BookingForm;
