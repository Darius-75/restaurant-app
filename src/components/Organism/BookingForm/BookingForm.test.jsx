import { render, screen } from "@testing-library/react";
import BookingForm from "./BookingForm";

test("Renders the submit button text", () => {
  const availableTimes = [
    { time: "10:00am" },
    { time: "12:00pm" },
    { time: "02:00pm" },
    { time: "04:00pm" },
    { time: "06:00pm" },
    { time: "08:00pm" },
    { time: "10:00pm" },
  ];
  render(<BookingForm availableTimes={availableTimes} />);
  const buttonTextElement = screen.getByText("Make Your reservation");
  expect(buttonTextElement).toBeInTheDocument();
});
