import React from "react";
import styled from "styled-components";

type Props = {
  height?: string;
  width?: string;
  handleClick?: any;
  color?: string;
  value?: string;
  children?: any;
  borderRadius?: string;
  type?: "button" | "submit" | "reset";
  style?: any;
  padding?: string;
  margin?: string;
  textAlign?: string;
  justifyContent?: string;
  display?: string;
  alignItems?: string;
  alignSelf?: string;
  justifySelf?: string;
  cursor?: string;
};

const StyleButton = styled.div<Props>`
  height: ${(props) => props.height};
  width: ${(props) => props.width};
  border-radius: ${(props) => props.borderRadius};
  background-color: ${(props) => props.color};
  padding: ${(props) => props.padding};
  margin: ${(props) => props.margin};
  display: ${(props) => props.display};
  align-items: ${(props) => props.alignItems};
  align-self: ${(props) => props.alignSelf};
  text-align: ${(props) => props.textAlign};
  cursor: ${(props) => props.cursor};
  justify-content: ${(props) => props.justifyContent};
  justify-self: ${(props) => props.justifySelf};
`;

const Button = ({
  height,
  width,
  borderRadius,
  color,
  handleClick,
  children,
  type,
  style,
  padding,
  margin,
  textAlign,
  justifyContent,
  display,
  alignItems,
  cursor,
  alignSelf,
  justifySelf,
}: Props) => {
  return (
    <StyleButton
      color={color}
      onClick={handleClick}
      height={height}
      width={width}
      borderRadius={borderRadius}
      type={type}
      style={style}
      padding={padding}
      margin={margin}
      display={display}
      alignItems={alignItems}
      alignSelf={alignSelf}
      textAlign={textAlign}
      justifyContent={justifyContent}
      justifySelf={justifySelf}
      cursor={cursor}
    >
      {children}
    </StyleButton>
  );
};

export default Button;
