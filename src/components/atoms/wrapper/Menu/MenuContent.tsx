import React from "react";
import styled from "styled-components";
import { colors, size } from "../../../../Variables/globalStyles";
import Button from "../../Button/Button";
import Text from "../../Text/Text";
import MenuSection from "../../../Organism/MenuSection/MenuSection";
import menu1 from "../../../../img/menu1.jpg";
import menu2 from "../../../../img/menu2.jpg";
import menu3 from "../../../../img/menu3.jpg";
const { Primary, Secondary } = colors;
const { XLMax, Max, Min, Normal } = size;
type Props = {};

const MenuContent = (props: Props) => {
  return (
    <div>
      <MenuContentGlobalDisplay>
        <MenuContainer>
          <TopSectionDisplay>
            <Text value={"This weeks specials !"} size={XLMax} />
            <Button
              color={Secondary}
              width="max-content"
              height="max-content"
              padding="10px 30px"
              borderRadius="16px"
            >
              <Text value={"Online Menu"} />
            </Button>
          </TopSectionDisplay>

          <MenuSectionDisplay>
            <MenuSection image={menu1} title="Greek Salade" price="12.99$" />
            <MenuSection image={menu2} title=" row fish" price="19.99$" />
            <MenuSection image={menu3} title="bowl Cake" price="9.99$" />
          </MenuSectionDisplay>
        </MenuContainer>
      </MenuContentGlobalDisplay>
    </div>
  );
};

const MenuContentGlobalDisplay = styled.div`
  height: 100vh;
  width: 100%;
  background-color: "#fff";
  display: flex;
  justify-content: center;
  align-items: center;
`;

const MenuContainer = styled.div`
  height: 70%;
  width: 60%;
  display: flex;
  flex-direction: column;
`;

const TopSectionDisplay = styled.div`
  height: max-content;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const MenuSectionDisplay = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 40px 0px;
`;

export default MenuContent;
