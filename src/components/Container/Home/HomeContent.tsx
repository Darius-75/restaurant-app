import React from "react";
import styled from "styled-components";
import { colors, size } from "../../../Variables/globalStyles";
import Text from "../../atoms/Text/Text";
import Icon from "../../atoms/Icon/Icon";
import homeImg from "../../../img/homeImg.jpg";
import Button from "../../atoms/Button/Button";
const { Primary, Secondary } = colors;
const { XLMax, Max, Min, Normal } = size;
type Props = {};

const HomeContent = (props: Props) => {
  return (
    <div>
      <GlobalHomeContainer>
        <HomeContentDisplay>
          <LeftTextSection>
            <Text value={"Little Lemon"} size={XLMax} color={Secondary} />
            <Text value={"Chicago"} size={Max} color={"#fff"} />
            <Text
              value={
                "A brief description of what type of food it offers A brief description of what typedescription of what type of food it offer"
              }
              color={"#fff"}
            />
            <Button
              color={Secondary}
              width="max-content"
              height="max-content"
              padding="10px 30px"
              borderRadius="16px"
            >
              <Text value={"Reserve a Table"} />
            </Button>
          </LeftTextSection>
          <RightImgSection>
            <Icon
              src={homeImg}
              width="100%"
              height="100%"
              borderRadius="10px"
            />
          </RightImgSection>
        </HomeContentDisplay>
      </GlobalHomeContainer>
    </div>
  );
};

const GlobalHomeContainer = styled.div`
  height: 40vh;
  width: 100%;
  background-color: ${Primary};
  display: flex;
  justify-content: center;
  align-items: center;
`;

const HomeContentDisplay = styled.div`
  height: 80%;
  width: 60%;
  display: flex;
  flex-direction: row;
  position: relative;
`;

const LeftTextSection = styled.div`
  height: 100%;
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const RightImgSection = styled.image`
  position: absolute;
  right: 0;
  width: ${({ width }) => width || "auto"};
  height: ${({ height }) => height || "150%"};
  object-fit: contain;
`;

export default HomeContent;
