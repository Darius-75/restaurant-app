import React, { useReducer } from "react";
import styled from "styled-components";
import { colors, size } from "../../../Variables/globalStyles";
import tee3 from "../../../img/tee3.jpg";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import Input from "../../atoms/Input/Input";
import Button from "../../atoms/Button/Button";
import BookingForm from "../../Organism/BookingForm/BookingForm";

const { Primary, Secondary } = colors;
const { XLMax, Max, Min, Normal } = size;

const availableTimes = [
  { time: "17:00 " },
  { time: "18:00 " },
  { time: "19:00 " },
  { time: "20:00 " },
  { time: "21:00 " },
  { time: "22:00 " },
];

const UPDATE_TIMES = "UPDATE_TIMES";

const initialState = {
  availableTimes,
};

const reducer = (state, action) => {
  switch (action.type) {
    case UPDATE_TIMES:
      return { availableTimes: action.payload };
    default:
      return state;
  }
};

const initializeTimes = (initialTimes) => {
  return { availableTimes: initialTimes };
};

const OrderContent = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const handleDateChange = (event) => {
    const date = event.target.value;
    const newTimes = [...state.availableTimes];
    dispatch({ type: UPDATE_TIMES, payload: newTimes });
  };

  return (
    <OrderContentGlobalDisplay>
      <BookingForm
        availableTimes={state.availableTimes}
        handleDateChange={handleDateChange}
      />
    </OrderContentGlobalDisplay>
  );
};

const OrderContentGlobalDisplay = styled.div`
  height: 100vh;
  width: 100%;
  background-image: url(${tee3});
  background-size: cover;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default OrderContent;
