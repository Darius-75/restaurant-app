import React from "react";
import styled from "styled-components";
import Text from "../../atoms/Text/Text";
import { colors, size } from "../../../Variables/globalStyles";
import footerImg from "../../../img/footerImg.jpg";
import Icon from "../../atoms/Icon/Icon";
const { Primary, Secondary } = colors;
const { Max, Min, Normal } = size;
type Props = {};

const FooterContent = (props: Props) => {
  return (
    <div>
      <GlobalContainer>
        <SectionsContainer>
          <ImgContainer>
            <Icon src={footerImg} width="100%" height="100%" />
          </ImgContainer>
          <MenusContainer>
            <Menu>
              <Text
                color={"#fff"}
                value={"Home"}
                size={Max}
                fontWeight="bold"
              />
              <Text color={"#fff"} value={"About"} />
              <Text color={"#fff"} value={"Menu"} />
              <Text color={"#fff"} value={"Reservation"} />
              <Text color={"#fff"} value={"Order"} />
              <Text color={"#fff"} value={"Login"} />
            </Menu>
            <Menu2>
              <Text
                color={"#fff"}
                value={"Contact"}
                fontWeight="bold"
                size={Max}
              />
              <Text color={"#fff"} value={"Adress"} />
              <Text color={"#fff"} value={"Phone number"} />
              <Text color={"#fff"} value={"Email"} />
            </Menu2>
            <Menu2>
              <Text
                color={"#fff"}
                value={"Social Media "}
                size={Max}
                fontWeight="bold"
              />
              <Text color={"#fff"} value={"Insta"} />
              <Text color={"#fff"} value={"TikTok"} />
              <Text color={"#fff"} value={"Twitter"} />
            </Menu2>
          </MenusContainer>
        </SectionsContainer>
      </GlobalContainer>
    </div>
  );
};

const GlobalContainer = styled.div`
  height: 50vh;
  background-color: ${Primary};
  display: flex;
  justify-content: center;
  padding: 30px;
`;
const SectionsContainer = styled.div`
  width: 60%;
  height: 100%;
  display: flex;
  justify-content: space-around;
`;

const ImgContainer = styled.div`
  height: 100%;
`;
const MenusContainer = styled.div`
  width: 60%;

  display: grid;
  grid-template-columns: repeat(3, 1fr);
  align-items: baseline; /* ou "baseline" si vous voulez aligner la base des textes */
`;

const Menu = styled.div`
  height: 100%;
  display: grid;
`;

const Menu2 = styled.div`
  height: 66%;
  display: grid;
`;

export default FooterContent;
