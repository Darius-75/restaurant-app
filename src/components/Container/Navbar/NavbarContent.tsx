import React from "react";
import styled from "styled-components";
import Text from "../../atoms/Text/Text";
import Icon from "../../atoms/Icon/Icon";
import Logo from "../../../img/icon.png";

type Props = {};

const NavbarContent = (props: Props) => {
  return (
    <div>
      <GlobalDisplay>
        <CenterDisplay>
          <IConSection>
            <Icon src={Logo} width="100% " height="80%" />
          </IConSection>
          <MenuSection>
            <Text value={"HOME"} fontWeight="semi-bold" />
            <Text value={"ABOUT"} fontWeight="semi-bold" />
            <Text value={"MENU"} fontWeight="semi-bold" />
            <Text value={"RESERVATION"} fontWeight="semi-bold" />
            <Text value={"ORDER "} fontWeight="semi-bold" />
            <Text value={"LOGIN"} fontWeight="semi-bold" />
          </MenuSection>
        </CenterDisplay>
      </GlobalDisplay>
    </div>
  );
};
const GlobalDisplay = styled.div`
  height: 80px;
  width: 100%;
  display: flex;
  justify-content: center;
`;

const CenterDisplay = styled.div`
  height: 100%;
  width: 60%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const IConSection = styled.div`
  height: 100%;
  width: 20%;
  display: flex;
  align-items: center;
`;
const MenuSection = styled.div`
  height: 100%;
  width: 50%;
  justify-content: space-between;
  display: flex;
  align-items: center;
`;

export default NavbarContent;
