export const colors = {
  Primary: "#495E57",
  Secondary: "#F4CE14",
};

export const size = {
  XLMax: "40px",
  Max: "24px",
  Normal: "16px",
  Min: "14px",
};
